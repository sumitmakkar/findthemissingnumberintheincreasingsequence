#include<iostream>
#include<vector>

using namespace std;

class Engine
{
private:
	   vector<int> arrVector;
    
	   bool checkIfNumberIsCorrectlyPlaced(int num , int index)
	   {
           return(num-1 == index ? true : false);
       }
    
public:
	   Engine(vector<int> aV)
	   {
           arrVector = aV;
       }
    
	   int findMissingNumber()
	   {
           int start = 0;
           int end   = (int)arrVector.size() - 1;
           while(start < end)
           {
               int mid = (start + end)/2;
               if(!checkIfNumberIsCorrectlyPlaced(arrVector[mid] , mid))
               {
                   end   = mid;
               }
               else
               {
                   start = mid + 1;
               }
           }
           return (arrVector[start]-1);
       }
};

int main()
{
    int arr[] = {1,2,3,4,5,6,7,8,10};
    int len   = (int)sizeof(arr)/sizeof(arr[0]);
    vector<int> arrVector;
    for(int i = 0 ; i < len ; i++)
    {
        arrVector.push_back(arr[i]);
    }
    Engine e = Engine(arrVector);
    cout<<e.findMissingNumber()<<endl;
    return 0;
}
